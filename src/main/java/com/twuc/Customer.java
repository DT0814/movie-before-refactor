package com.twuc;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Customer {
    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental rental) {
        rentals.add(rental);
    }

    private String getName() {
        return name;
    }

    public String statement() {
        StringBuilder result = new StringBuilder("Rental Record for " + getName() + "\n");
        printRentalDetail(result);
        result.append("Amount owed is ").append(getTotalAmount()).append("\n");
        result.append("You earned ").append(getFrequentRenterPoints()).append(" frequent renter points");
        return result.toString();
    }

    private void printRentalDetail(StringBuilder result) {
        AtomicInteger frequentRenterPoints = new AtomicInteger();
        rentals.forEach(rental -> {
            frequentRenterPoints.addAndGet(rental.getFrequentRenterPoints());
            result.append("\t").append(rental.getMovie().getTitle()).append("\t").append(rental.getCharge()).append("\n");
        });
    }

    private long getFrequentRenterPoints() {
        return rentals.stream().map(Rental::getFrequentRenterPoints).reduce(0, Integer::sum);
    }

    private double getTotalAmount() {
        return rentals.stream().map(Rental::getCharge).reduce((double) 0, Double::sum);
    }

    public String statementInHtmlFormat() {
        return null;
    }
}
