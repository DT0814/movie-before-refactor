package com.twuc;

public class Rental {
    private Movie movie;
    private int daysRented;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    private int getDaysRented() {
        return daysRented;
    }

    Movie getMovie() {
        return movie;
    }

    int getFrequentRenterPoints() {
       return getMovie().getFrequentRenterPoints(getDaysRented());
    }

    double getCharge() {
        return getMovie().getCharge(getDaysRented());
    }
}
